FROM ubuntu:16.04 AS fly-php7.2
RUN set -xe 		\
    && echo '#!/bin/sh' > /usr/sbin/policy-rc.d 	\
    && echo 'exit 101' >> /usr/sbin/policy-rc.d 	\
    && chmod +x /usr/sbin/policy-rc.d 		\
    && dpkg-divert --local --rename --add /sbin/initctl 	\
    && cp -a /usr/sbin/policy-rc.d /sbin/initctl 	\
    && sed -i 's/^exit.*/exit 0/' /sbin/initctl 		\
    && echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup 		\
    && echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean 	\
    && echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean 	\
    && echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean 		\
    && echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages 		\
    && echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes 		\
    && echo 'Apt::AutoRemove::SuggestsImportant "false";' > /etc/apt/apt.conf.d/docker-autoremove-suggests && \
    sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list && \
    mkdir -p /run/systemd && \
    echo 'docker' > /run/systemd/container && \
    apt update && \
    apt install -y \
        software-properties-common \
        curl \
        wget && \
    export LC_ALL=C.UTF-8 && \
    add-apt-repository --yes ppa:nginx/stable && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -my \
        nginx-full \
        gettext-base \
        php7.2 \
        php7.2-fpm \
        php7.2-curl \
        php7.2-json \
        php7.2-mbstring \
        php7.2-opcache \
        php7.2-xml \
        php7.2-mysql \
        php7.2-pgsql \
        php7.2-memcached \
        supervisor \
        composer \
        cron \
        unzip && \
    sed -i s'/listen = \/run\/php\/php7.2-fpm.sock/listen = 9000/' /etc/php/7.2/fpm/pool.d/www.conf && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    mkdir -p /var/run/php && \
    mkdir -p /var/www/public && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/*
COPY docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/default.conf /etc/nginx/conf.d/default.conf

# Boot
WORKDIR /var/www

CMD ["/usr/bin/supervisord"]

# Expose ports
EXPOSE 80

# --------------------------------------------------

# Base image

FROM fly-php7.2

# Mount to www
#COPY --chown=www-data:www-data . /var/www
COPY . /var/www
RUN chown -R www-data:www-data /var/www


RUN composer global require hirak/prestissimo && composer install --no-progress
RUN apt-get update && \
    apt-get install -y php7.2-xdebug && \
    rm -rf /var/lib/apt/lists/*
