
// Import TinyMCE
import tinymce from 'tinymce/tinymce';

// A theme is also required
import 'tinymce/themes/silver/theme';

// Any plugins you want to use has to be imported
import 'tinymce/plugins/paste';
import 'tinymce/plugins/link';

// $('.selectpicker').selectpicker();

$(document).ready(function () {
    $('#message-table').DataTable();
});


tinymce.init({
    selector: '#text-message',
    plugins: ['paste', 'link']
});
