import Vue from 'vue'

window.Vue = Vue

import tinymce from 'vue-tinymce-editor'

Vue.component('MyEditor', require('./components/editor/editor.vue').default);
Vue.component('DataTable', require('./components/massage/index').default);

const app = new Vue({
    el: '#wrapper',
    data: {
        data: '',
        fillItem : {'title':'','description':'','id':''},
    },
    components: {
        tinymce,
    }
})