import React, {Component} from 'react';
import ReactDOM from "react-dom";
import Tickets from "./Tickets";
import TopDestination from "./TopDestination";

class SearchTickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            from: '',
            to: '',
            departure: '',
            return: '',

            topDestination: true,
            tickets: false,
            ticketsArray: []
        };

        // this.handleChangeFrom = this.handleChangeFrom.bind(this);
        // this.handleChangeTo = this.handleChangeTo.bind(this);
        // this.handleChangeDeparture = this.handleChangeDeparture.bind(this);
        // this.handleChangeReturn = this.handleChangeReturn.bind(this);
    }
    componentDidMount() {
        fetch('/fly/airport')
            .then(function(response) {
                return response.json();
            })
            .then((result) => {
                this.setState({ options: result });
            })
            .catch( alert );

        $(ReactDOM.findDOMNode(this.refs.inputDeparture)).datepicker({
            format: 'dd/mm/yyyy'
        })
        var _this = this;
        $(ReactDOM.findDOMNode(this.refs.inputDeparture)).on('changeDate', function(e) {
            _this.handleChangeDeparture(e);
        })
        $(ReactDOM.findDOMNode(this.refs.inputReturn)).datepicker({
            format: 'dd/mm/yyyy'
        })
        var _this = this;
        $(ReactDOM.findDOMNode(this.refs.inputReturn)).on('changeDate', function(e) {
            _this.handleChangeReturn(e);
        })
    }
    componentWillMount(){
        $(this.refs.inputDeparture).datepicker('destroy');
        $(this.refs.inputReturn).datepicker('destroy');
    }

    handleChangeFrom = (event) => {
        this.state.from = event.target.value;
        this.search();
    }

    handleChangeTo = (event) => {
        this.state.to = event.target.value;
        this.search();
    }

    handleChangeDeparture = (event) => {
        this.state.departure = event.target.value;
        this.search();
    }

    handleChangeReturn = (event) => {
        this.state.return = event.target.value;
        this.search();
    }

    search = () => {
        if (this.state.from.length == 0 || this.state.to.length == 0 ||
            this.state.departure.length == 0 || this.state.return.length == 0
        ) {
            return false;
        }
        const form = {
            from: this.state.from,
            to: this.state.to,
            departure: this.state.departure,
            return: this.state.return,
        };

        fetch("/fly/search",
            {
                method: "POST",
                body: JSON.stringify(form)
            })
            .then(function(response) {
                return response.json();
            })
            .then((result) => {
                this.setState({
                    tickets: true,
                    topDestination: false,
                    ticketsArray: result.data
                });

            })
        .catch(function(res){ console.log(res) })
    }


    render () {
        const options = this.state.options;
        let block = null;
        if (this.state.topDestination) {
            block = <TopDestination/>;
        } else if(this.state.tickets) {
            block = <Tickets tickets={this.state.ticketsArray}/>;
        }

        return (
            <div>
                <div id="search-form">
                    <div className="row detail-filter-wrap">
                        <div className="col-md-6">
                            <select value={this.state.from} className="form-control selectpicker"
                                    data-live-search="true"
                                    data-size="10" onChange={this.handleChangeFrom}>
                                {options.map(item => (
                                    <option key={item.id} value={item.iata}>
                                        {item.city}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="col-md-6">
                            <select value={this.state.to} className="form-control selectpicker" data-live-search="true"
                                    data-size="10" onChange={this.handleChangeTo}>
                                {options.map(item => (
                                    <option key={item.id} value={item.iata}>
                                        {item.city}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="col-md-6 mt-3">
                            <input type="text" ref="inputDeparture" className="form-control datepicker"
                                   placeholder="Departure" value={this.state.departure}
                                   onChange={this.handleChangeDeparture}/>
                        </div>
                        <div className="col-md-6 mt-3">
                            <input type="text" ref="inputReturn" className="form-control datepicker"
                                   placeholder="Return" value={this.state.return} onChange={this.handleChangeReturn}/>
                        </div>
                    </div>
                </div>
                <div className="row detail-filter-wrap filter-block">
                    <div className="col-md-12">
                        <div className="button_line_1">
                            <button className="btn btn-default btn-sm">Transport</button>
                            <button className="btn btn-default btn-sm">Stops</button>
                            <button className="btn btn-default btn-sm">Price</button>
                        </div>
                    </div>
                </div>
                <div className="row mt-3">
                    {block}
                </div>
            </div>
        )
    }
}

ReactDOM.render(<SearchTickets/>, document.getElementById('wrapper'));