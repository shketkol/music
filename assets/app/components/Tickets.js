import React, {Component} from 'react';

class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ticketsArray: []
        };

    }

    render () {
        const tickets = this.props.tickets;

        return (
            <div>
                <div className="col-12">
                    <ul className="choose-item">
                        <li className="active">
                            <div>
                                <span className="font-weight-bold">Best</span>
                                <br/>
                                <span><b>76$</b> 4h 10m</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p className="font-weight-bold">Cheapest</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p className="font-weight-bold">Fastest</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="col-12">
                    <div className="tickets">
                        {tickets.map(item => (
                            <div className="ticket" key={item.id}>
                                <div className="main-info">
                                    <div className="price">
                                        <p className="font-weight-bold">{item.price}$</p>
                                    </div>
                                    <div className="airlines mt-3">
                                        <div className="airline-item">
                                            <img src="https://images.kiwi.com/airlines/32x32/.png" alt=""/>
                                            <span className="small">{item.route[0].airline}</span>
                                        </div>
                                        <hr/>
                                        <div className="airline-item mt-3">
                                            <img src="https://images.kiwi.com/airlines/32x32/PS.png" alt=""/>
                                            <span className="small">{item.route[1].airline}</span>
                                        </div>
                                    </div>
                                    <div className="time mt-3 ml-2">
                                        <span className="font-weight-bold">09:50 - 11:55</span>
                                        <br/>
                                        <span className="small">Wed 21 Aug</span>
                                        <br/>
                                        <span className="font-weight-bold mt-2">09:50 - 11:55</span>
                                        <br/>
                                        <span className="small">Wed 21 Aug</span>
                                    </div>
                                    <div className="duration mt-3 ml-3">
                                        <span>{item.fly_duration}</span>
                                        <br/>
                                        <span className="small">Kiev (KBP)‎ Helsinki (HEL)‎</span>
                                    </div>
                                    <div className="arrow">
                                        <a className="" data-toggle="collapse" href="#collapseExample" role="button"
                                           aria-expanded="false" aria-controls="collapseExample">Arrow</a>
                                    </div>
                                </div>
                                <div className="collapse" id="collapseExample">
                                    <div className="card card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                        cred nesciunt sapiente ea proident.
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}

export default Tickets;