<?php
namespace App\Command;

use App\Entity\Country;
use App\Entity\KiwiAirport;
use App\Services\Client\Kiwi\ApiClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AirportsAsiaCommand extends Command
{
    private $container;
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:airport-asia';

    protected function configure()
    {
    }

    public function __construct($name = null, ContainerInterface $container)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->container->get('doctrine')->getManager();
        $country = $this->container->get('doctrine')
            ->getRepository(Country::class)
            ->getCountryAsia();

        foreach ($country as $key => $value) {
            $params = [
                'type' => 'subentity',
                'term' => $value['code'],
                'locale' => 'uk-UA',
                'active_only' => true,
                'location_types' => 'airport',
                'limit' => 100,
                'sort' => 'name'
            ];

            $send = ApiClient::send('GET', 'locations', $params);
            $send = $send['locations'] ?? null;
            if (!empty($send)) {
                foreach ($send as $k => $v) {
                    $kiwiAirport = new KiwiAirport();
                    $kiwiAirport->setCode($v['id'] ?? null);
                    $kiwiAirport->setCountryId($value['country_id']);
                    $kiwiAirport->setIntId($v['int_id'] ?? null);
                    $kiwiAirport->setName($v['name'] ?? null);
                    $kiwiAirport->setSlug($v['slug'] ?? null);
                    $kiwiAirport->setRank($v['rank'] ?? null);
                    $kiwiAirport->setTimezone($v['timezone'] ?? null);
                    $kiwiAirport->setCity($v['city'] ?? null);
                    $kiwiAirport->setLocation($v['location'] ?? null);
                    $kiwiAirport->setTags($v['tags'] ?? null);
                    $entityManager->persist($kiwiAirport);
                    $entityManager->flush();
                }
            }
        }
    }
}