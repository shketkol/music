<?php
namespace App\Command;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\KiwiAirport;
use App\Services\CityService;
use App\Services\Client\Kiwi\ApiClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AirportsEuropeCommand extends Command
{
    private $container;

    private $entityManager;
    private $countyRepository;
    private $cityRepository;
    private $cityService;

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:airport-europe';

    protected function configure()
    {
    }

    public function __construct($name = null, ContainerInterface $container, CityService $cityService)
    {
        parent::__construct($name);
        $this->container = $container;
        $this->entityManager = $this->container->get('doctrine')->getManager();
        $this->countyRepository = $this->container->get('doctrine')
            ->getRepository(Country::class);
        $this->cityRepository = $this->container->get('doctrine')
            ->getRepository(City::class);
        $this->cityService = $cityService;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $country = $this->countyRepository->getCountryEurope();

        foreach ($country as $key => $value) {
            $params = [
                'type' => 'subentity',
                'term' => $value['code'],
                'locale' => 'en-US',
                'active_only' => true,
                'location_types' => 'airport',
                'limit' => 100,
                'sort' => 'name'
            ];

            $send = ApiClient::send('GET', 'locations', $params);
            $send = $send['locations'] ?? null;
            if (!empty($send)) {
                foreach ($send as $k => $v) {
                    $city = $this->cityRepository->getCity($v['city']['code']);
                    if (empty($city)) {
                        $params = [
                            'term' => $v['city']['code'],
                            'locale' => 'en-US',
                            'active_only' => true,
                            'location_types' => 'city',
                            'limit' => 10,
                            'sort' => 'name'
                        ];
                        $cityResponse = ApiClient::send('GET', 'locations', $params);
                        $cityResponse = $cityResponse['locations'] ?? null;
                        if (empty($cityResponse)) {
                            continue;
                        }

                        foreach ($cityResponse as $cK => $cV) {
                            if ($cV['code'] == $v['city']['code']) {
                                $city = $this->cityService->saveCity([
                                    'kiwiId' => $cV['id'],
                                    'name' => $cV['name'],
                                    'kiwiSlug' => $cV['slug'],
                                    'code' => $cV['code'],
                                    'timeZone' => $cV['timezone'],
                                    'countryId' => $value['id'],
                                    'location' => $cV['location']
                                ]);
                            }
                        }
                    }

                    $kiwiAirport = new KiwiAirport();
                    $kiwiAirport->setCode($v['id'] ?? null);
                    $kiwiAirport->setCountryId($value['id']);
                    $kiwiAirport->setCityId($city->getId());
                    $kiwiAirport->setIntId($v['int_id'] ?? null);
                    $kiwiAirport->setName($v['name'] ?? null);
                    $kiwiAirport->setSlug($v['slug'] ?? null);
                    $kiwiAirport->setRank($v['rank'] ?? null);
                    $kiwiAirport->setTimezone($v['timezone'] ?? null);
                    $kiwiAirport->setCity($v['city'] ?? null);
                    $kiwiAirport->setLocation($v['location'] ?? null);
                    $kiwiAirport->setTags($v['tags'] ?? null);
                    $this->entityManager->persist($kiwiAirport);
                }
                $this->entityManager->flush();
            }
        }
    }
}