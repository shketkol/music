<?php
namespace App\Controller;

use App\Entity\Airport;
use App\Entity\KiwiAirport;
use App\Entity\Traits\KiwiApi;
use App\Repository\AirportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FlyController extends Controller
{
	use KiwiApi;

    public function index()
    {
        $airports = $this->getDoctrine()
            ->getRepository(KiwiAirport::class)
            ->findBy([], null, 10, 0);

        $departures = $this->getDoctrine()
            ->getRepository(Airport::class)
            ->getAirport();
        array_unshift($departures, [
            'iata' => 'anywhere',
            'city' => 'Anywhere'
        ]);

        return $this->render('fly/index.html.twig', [
            'airports' => $airports,
            'departures' => $departures
        ]);
    }

    public function airport()
    {
        $departures = $this->getDoctrine()
            ->getRepository(Airport::class)
            ->getAirport();
        array_unshift($departures, [
            'id' => 0,
            'iata' => 'anywhere',
            'city' => 'Anywhere'
        ]);
        return $this->json($departures);
    }

    public function search(Request $request)
    {
        $answer = file_get_contents('php://input');

        if (!empty($answer)) {
            $answer = json_decode($answer, true);
            $from = $answer['from'];
            $to = $answer['to'];
            $departures = $answer['departure'];
            $return = $answer['return'];

            $params = [
                'fly_from' => $from,
                'fly_to' => $to,
                'date_from' => $departures,
                'date_to' => $return
            ];

            $send = $this->send('GET', 'flights', $params);
            return $this->json($send);
        }
    }


    public function dump()
	{
		$file = $_SERVER['DOCUMENT_ROOT'] . '/dump';
		$entityManager = $this->getDoctrine()->getManager();


		foreach(file($file) as $row) {
			$array = explode(',', $row);
			$airport = new Airport();
			$airport->setName(str_replace('"', '', $array[1]));
			$airport->setCity(str_replace('"', '', $array[2]));
			$airport->setCountry(str_replace('"', '', $array[3]));
			$airport->setIata(str_replace('"', '', $array[4]));
			$airport->setIcao(str_replace('"', '', $array[5]));
			$airport->setLat(str_replace('"', '', $array[6]));
			$airport->setLng(str_replace('"', '', $array[7]));
			$airport->setTimezone(str_replace('"', '', $array[11]));
			$entityManager->persist($airport);
			$entityManager->flush();

			$params = [
				'type' => 'id',
				'id' => str_replace('"', '', $array[4]),
				'locale' => 'en-US',
				'active_only' => true
			];

			$send = $this->send('GET', 'locations', $params);
			$send = $send['locations'][0] ?? null;
			if (!empty($send['name'])) {
				$kiwiAirport = new KiwiAirport();
				$kiwiAirport->setCode($send['id'] ?? null);
				$kiwiAirport->setIntId($send['int_id'] ?? null);
				$kiwiAirport->setName($send['name'] ?? null);
				$kiwiAirport->setSlug($send['slug'] ?? null);
				$kiwiAirport->setRank($send['rank'] ?? null);
				$kiwiAirport->setTimezone($send['timezone'] ?? null);
				$kiwiAirport->setCity($send['city'] ?? null);
				$kiwiAirport->setLocation($send['location'] ?? null);
				$kiwiAirport->setTags($send['tags'] ?? null);
				$entityManager->persist($kiwiAirport);
				$entityManager->flush();
			}
		}
		$entityManager->flush();

		var_dump(file_exists($file));



		die;
	}

}