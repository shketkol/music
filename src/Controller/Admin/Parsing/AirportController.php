<?php
namespace App\Controller\Admin\Parsing;

use App\Controller\AdminController;
use App\Entity\Country;
use App\Entity\KiwiAirport;
use App\Services\Client\Kiwi\ApiClient;

class AirportController extends AdminController
{
    public function getAirport()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $country = $this->getDoctrine()
            ->getRepository(Country::class)
            ->getCountryEurope();

        foreach ($country as $key => $value) {
            $params = [
                'type' => 'subentity',
                'term' => $value['iso'],
                'locale' => 'en-US',
                'active_only' => true,
                'location_types' => 'airport',
                'limit' => 100,
                'sort' => 'name'
            ];

            $send = ApiClient::send('GET', 'locations', $params);
            $send = $send['locations'] ?? null;
            if (!empty($send)) {
                foreach ($send as $k => $v) {
                    $kiwiAirport = new KiwiAirport();
                    $kiwiAirport->setCode($v['id'] ?? null);
                    $kiwiAirport->setCountryId($value['id']);
                    $kiwiAirport->setIntId($v['int_id'] ?? null);
                    $kiwiAirport->setName($v['name'] ?? null);
                    $kiwiAirport->setSlug($v['slug'] ?? null);
                    $kiwiAirport->setRank($v['rank'] ?? null);
                    $kiwiAirport->setTimezone($v['timezone'] ?? null);
                    $kiwiAirport->setCity($v['city'] ?? null);
                    $kiwiAirport->setLocation($v['location'] ?? null);
                    $kiwiAirport->setTags($v['tags'] ?? null);
                    $entityManager->persist($kiwiAirport);
                    $entityManager->flush();
                }
            }
        }



    }
}