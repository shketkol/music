<?php
namespace App\Controller\Admin\Message;

use App\Controller\AdminController;
use Symfony\Component\HttpFoundation\Request;
use TelegramBot\Api\BotApi;

class IndexController extends AdminController
{
    public function index()
    {
        return $this->render('message/index/index.html.twig');
    }

    public function create(Request $request)
    {
        if ($request->getPort()) {
            var_dump(11);
        }

        return $this->render('message/index/create.html.twig');
    }
}