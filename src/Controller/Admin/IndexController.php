<?php
namespace App\Controller\Admin;

use App\Controller\AdminController;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Client;

class IndexController extends AdminController
{
    public function index()
    {

        return $this->render('index/index.html.twig');
    }

    public function test()
    {
        $bot = new BotApi($this->container->getParameter('telegram_token'));

        $str = "
            Проведи весну в Барселонію Лиліт з Києва 15 квітня від 31 Євро!
            https://www.kiwi.com/ru/poisk/%D0%BA%D0%B8%D0%B5%D0%B2-%D1%83%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0/%D0%B1%D0%B0%D1%80%D1%81%D0%B5%D0%BB%D0%BE%D0%BD%D0%B0-%D0%B8%D1%81%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/anytime/no-return
            
            Летимо на выдпочинок на початку сезону. Більше інформації за посиланням вище.
        ";

        $bot->sendMessage('@footinhand', $str);
        return 'ok';
    }
}