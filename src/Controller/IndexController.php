<?php
namespace App\Controller;

use App\Entity\Traits\FApi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    use FApi;

    public function index()
    {

        return $this->render('index/index.html.twig');
    }

    public function test()
    {

        return $this->render('fly/index.html.twig');
    }

    public function category()
    {
        $result = $this->send('GET', 'venues/categories');

        $this->setCategory($result['response']['categories']);
    }

    private function setCategory(array $data, ?int $parent=null)
    {
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($data as $value){
            $category = $this->getDoctrine()
                ->getRepository(FCategory::class)
                ->findOneBy(['fid' => $value->id]);

            if (empty($category)) {
                $category = new FCategory();
                $category->setFid($value['id']);
                $category->setName($value['name']);
                $category->setPluralName($value['pluralName']);
                $category->setShortName($value['shortName']);
                $category->setParent($parent);
                $entityManager->persist($category);

                $entityManager->flush();
            }

            if (!empty($value['categories'])) {
                return $this->setCategory($value['categories'], $category->getId());
            }

        }


    }
}