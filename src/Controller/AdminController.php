<?php
namespace App\Controller;

use App\Entity\Traits\FApi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    protected $rootDirectory = 'admin/';

    public function render(string $view, array $parameters = array(), Response $response = null): Response
    {
        $view = $this->rootDirectory . $view;

        return parent::render($view, $parameters, $response);
    }
}