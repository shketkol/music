<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $file = include ('./assets/init/counries.php');

        $europe = include ('./assets/init/europe.php');
        $europe = explode(', ', $europe);

        foreach ($file as $key => $value) {
            $county = new Country();
            $county->setCode($key);
            $county->setName($value);
            if (array_search($key, $europe) !== false) {
                $county->setRegion('EU');
            }
            $manager->persist($county);
        }
        $manager->flush();
    }
}
