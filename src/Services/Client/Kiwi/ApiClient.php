<?php

namespace App\Services\Client\Kiwi;

use GuzzleHttp\Client;

class ApiClient
{
    /**
     * @param string $method
     * @param string $url
     * @param array $params
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function send(string $method, string $url, array $params=[]) :?array
    {
        try {
            $client = new Client(['base_uri' => 'https://api.skypicker.com/']);
            $result = $client->request($method, $url, [
                'query' => $params
            ]);

            if ($result->getStatusCode() == 200) {
                return json_decode($result->getBody()->getContents(), true);
            } else {
                return [];
            }
        } catch (\Exception $exception) {
            return [];
        }
    }
}