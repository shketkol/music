<?php

namespace App\Services;

use App\Entity\City;
use ArrayHelpers\Arr;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CityService
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function saveCity($attributes)
    {
        $city = new City();
        $city->setKiwiId(Arr::get($attributes, 'kiwiId'));
        $city->setName(Arr::get($attributes, 'name'));
        $city->setKiwiSlug(Arr::get($attributes, 'kiwiSlug'));
        $city->setCode(Arr::get($attributes, 'code'));
        $city->setTimeZone(Arr::get($attributes, 'timeZone'));
        $city->setCountryId(Arr::get($attributes, 'countryId'));
        $city->setLocation(Arr::get($attributes, 'location'));
        $this->em->persist($city);
        $this->em->flush();

        return $city;
    }
}
