<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190304145358 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE city (id INT NOT NULL, kiwi_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, kiwi_slug VARCHAR(255) NOT NULL, code VARCHAR(5) NOT NULL, time_zone TEXT NOT NULL, country_id INT NOT NULL, location JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE country (id INT NOT NULL, code VARCHAR(2) NOT NULL, name VARCHAR(80) NOT NULL, region VARCHAR(80) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE airport (id INT NOT NULL, name VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, iata VARCHAR(255) NOT NULL, icao VARCHAR(255) NOT NULL, lat VARCHAR(255) NOT NULL, lng VARCHAR(255) NOT NULL, timezone VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE kiwi_airport (id INT NOT NULL, code VARCHAR(255) NOT NULL, country_id INT NOT NULL, city_id INT NOT NULL, int_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, rank VARCHAR(255) NOT NULL, timezone VARCHAR(255) NOT NULL, city JSON NOT NULL, location JSON NOT NULL, tags JSON NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE airport');
        $this->addSql('DROP TABLE kiwi_airport');
    }
}
