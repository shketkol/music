<?php

namespace App\Repository;

use App\Entity\FCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method FCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method FCategory[]    findAll()
 * @method FCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FCategory::class);
    }

//    /**
//     * @return FCategory[] Returns an array of FCategory objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FCategory
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
