<?php

namespace App\Repository;

use App\Entity\KiwiAirport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method KiwiAirport|null find($id, $lockMode = null, $lockVersion = null)
 * @method KiwiAirport|null findOneBy(array $criteria, array $orderBy = null)
 * @method KiwiAirport[]    findAll()
 * @method KiwiAirport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KiwiAirportRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, KiwiAirport::class);
    }

//    /**
//     * @return KiwiAirport[] Returns an array of KiwiAirport objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KiwiAirport
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
