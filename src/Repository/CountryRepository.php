<?php

namespace App\Repository;

use App\Entity\Airport;
use App\Entity\Country;
use App\Entity\KiwiAirport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Airport|null find($id, $lockMode = null, $lockVersion = null)
 * @method Airport|null findOneBy(array $criteria, array $orderBy = null)
 * @method Airport[]    findAll()
 * @method Airport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function getCountryEurope()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.region = :continent_code')
            ->setParameter('continent_code', 'EU')
            ->getQuery()
            ->getArrayResult();
    }

    public function getCountryAsia()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.code = :continent_code')
            ->setParameter('continent_code', 'AS')
            ->getQuery()
            ->getArrayResult();
    }

    public function getCountryAmerica()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.code = :continent_code')
            ->setParameter('continent_code', 'NA')
            ->getQuery()
            ->getArrayResult();
    }
    public function getCountryOushen()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.continent_code = :continent_code')
            ->setParameter('continent_code', 'OC')
            ->getQuery()
            ->getArrayResult();
    }
}
