<?php

namespace App\Entity\Traits;

use GuzzleHttp\Client;

trait FApi
{
    /**
     * @param $method
     * @param $url
     * @param $params
     * @return array|null
     */
    public function send(string $method, string $url, array $params=[]) :?array
    {
        $paramsMain = [
            'client_id' => 'Y3GNHXN53TSEUFOTIEWCO0X3R3L4IGTY0ITOHNAK0OPXCTC0',
            'client_secret' => 'ENPC2ULMGQVQZREIJSFPQOR302OQ4N2DX3EO53D3SCTYZ0HB',
            'v' => date('Ymd', time()),
            'lang' => 'en'
        ];

        $params = array_merge($paramsMain, $params);

        $client = new Client(['base_uri' => 'https://api.foursquare.com/v2/']);
        $result = $client->request($method, $url, [
                   'query' => $params
                ]);

        if ($result->getStatusCode() == 200) {
            return json_decode($result->getBody()->getContents(), true);
        }
        return [];
    }
}