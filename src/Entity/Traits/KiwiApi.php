<?php

namespace App\Entity\Traits;

use GuzzleHttp\Client;

trait KiwiApi
{
    /**
     * @param $method
     * @param $url
     * @param $params
     * @return array|null
     */
    public function send(string $method, string $url, array $params=[]) :?array
    {

        $client = new Client(['base_uri' => 'https://api.skypicker.com/']);
        $result = $client->request($method, $url, [
                   'query' => $params
                ]);

        if ($result->getStatusCode() == 200) {
            return json_decode($result->getBody()->getContents(), true);
        }
        return [];
    }
}