<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KiwiAirportRepository")
 */
class KiwiAirport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $country_id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $city_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $int_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rank;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $timezone;

    /**
     * @ORM\Column(type="json")
     */
    private $city;

    /**
     * @ORM\Column(type="json")
     */
    private $location;

    /**
     * @ORM\Column(type="json")
     */
    private $tags;

    public function getId()
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode($code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getIntId(): ?int
    {
        return $this->int_id;
    }

    public function setIntId($int_id): self
    {
        $this->int_id = $int_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug($slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getRank(): ?string
    {
        return $this->rank;
    }

    public function setRank($rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone($timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getCity()
    {
        return json_decode($this->city, true);
    }

    public function setCity($city): self
    {
        $this->city = json_encode($city);

        return $this;
    }

    public function getLocation()
    {
        return json_decode($this->location, true);
    }

    public function setLocation($location): self
    {
        $this->location = json_encode($location);

        return $this;
    }

    public function getTags()
    {
        return json_decode($this->tags, true);
    }

    public function setTags($tags): self
    {
        $this->tags = json_encode($tags);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * @param mixed $country_id
     */
    public function setCountryId($country_id): void
    {
        $this->country_id = $country_id;
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param mixed $city_id
     */
    public function setCityId($city_id): void
    {
        $this->city_id = $city_id;
    }

}
