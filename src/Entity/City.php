<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 */
class City
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $kiwiId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $kiwiSlug;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $code;

    /**
     * @ORM\Column(type="text", length=255)
     */
    private $timeZone;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $countryId;

    /**
     * @ORM\Column(type="json")
     */
    private $location;

    /**
     * @return mixed
     */
    public function getKiwiId()
    {
        return $this->kiwiId;
    }

    /**
     * @param mixed $kiwiId
     */
    public function setKiwiId($kiwiId): void
    {
        $this->kiwiId = $kiwiId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getKiwiSlug()
    {
        return $this->kiwiSlug;
    }

    /**
     * @param mixed $kiwiSlug
     */
    public function setKiwiSlug($kiwiSlug): void
    {
        $this->kiwiSlug = $kiwiSlug;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param mixed $timeZone
     */
    public function setTimeZone($timeZone): void
    {
        $this->timeZone = $timeZone;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param mixed $countryId
     */
    public function setCountryId($countryId): void
    {
        $this->countryId = $countryId;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return json_decode($this->location, true);
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $this->location = json_encode($location);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

}
