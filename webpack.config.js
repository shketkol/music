const { VueLoaderPlugin } = require('vue-loader')
var Encore = require('@symfony/webpack-encore');

//DECLARATION OF THE NEW PLUGIN
var CopyWebpackPlugin = require('copy-webpack-plugin');


Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())

    // uncomment to define the assets of the project
     .addEntry('js/app_admin', [
         './assets/public/bootstrap/dist/js/bootstrap.min.js',
         './assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
         './assets/public/js/jquery.slimscroll.js',
         './assets/public/js/waves.js',
         './assets/public/js/custom.min.js',
         './assets/public/js/dashboard1.js',
         './assets/public/js/cbpFWTabs.js',
         './assets/plugins/bower_components/toast-master/js/jquery.toast.js',
         './assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
         './assets/plugins/bower_components/datatables/jquery.dataTables.min.js',

         './assets/js/main.js'
     ])

    // .addEntry('js/react', [
    //     // './assets/app/index.js',
    // ])
     .addEntry('js/vue', [
         './assets/app-admin/index.js',
     ])
     .addStyleEntry('css/app_admin', [
        './assets/public/bootstrap/dist/css/bootstrap.min.css',
        './assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
        './assets/plugins/bower_components/toast-master/css/jquery.toast.css',
        './assets/plugins/bower_components/datatables/jquery.dataTables.min.css',
        './assets/public/css/animate.css',
        './assets/public/css/style.css',
        './assets/public/css/colors/default.css'
     ])

     // Add react preset
    .enableReactPreset()

    // Enable Vue SFC compilation
    .enableVueLoader()

    // Tried with and without this
    .addPlugin(new VueLoaderPlugin())

    .configureBabel(function(babelConfig) {
        // add additional presets
        babelConfig.presets.push('es2015');
        babelConfig.presets.push('stage-0');
    })

    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    })

    .addPlugin(new CopyWebpackPlugin([
        // Copy the skins from tinymce to the build/skins directory
        { from: 'node_modules/tinymce/skins', to: 'js/skins' },
    ]))

    // uncomment if you use Sass/SCSS files
    // .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
     .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
