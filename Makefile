ENV=local
dir=${CURDIR}
project=-p fly
service=fly:latest

start:
	@docker-compose -f docker-compose.yml $(project) up -d

start-ci:
	@docker-compose -f docker-compose-ci.yml $(project) up -d

stop:
	@docker-compose -f docker-compose.yml $(project) down

stop-ci:
	@docker-compose -f docker-compose.ci.yml $(project) down

restart: stop start

env-local:
	cp ./.env.local ./.env

env-aws:
	cp ./.env.aws.dev ./.env

env-stage:
	cp ./.env.stage ./.env

env-prod:
	cp ./.env.prod ./.env

env-jenkins:
	cp ./.env.jenkins ./.env

ssh:
	@docker-compose $(project) exec php-fpm bash

ssh-node:
	@docker-compose $(project) exec node sh

logs-node:
	@docker-compose $(project) logs node

exec:
	@docker-compose $(project) exec php-fpm $$cmd

exec-true:
	@docker-compose $(project) exec php-fpm $$cmd || true

сhmod-project:
	@make exec cmd="chmod -R 777 *"

clean:
	rm -rf $(dir)/reports

prepare:
	mkdir $(dir)/reports
	mkdir $(dir)/reports/coverage

wait-for-db:
	@make exec cmd="php artisan db:wait"

composer-install-prod:
	@make exec cmd="composer install --no-dev"

composer-install:
	@make exec cmd="composer install"

composer-update:
	@make exec cmd="composer update"

info:
	@make exec cmd="php artisan --version"
	@make exec cmd="php --version"

truncate:
	@make exec cmd="php artisan db:truncate"
	@make exec cmd="php artisan db:truncate --env=testing"

migrate:
	@make exec cmd="php artisan migrate --force"
	@make exec cmd="php artisan migrate --force --env=testing"

config-cache:
	@make exec cmd="php artisan config:cache"

seed:
	@make exec cmd="php artisan db:seed --force"

dusk-reports:
	@make exec cmd="php artisan dusk -c phpunit.dusk.xml --log-junit reports/phpunit.xml --coverage-html reports/coverage --coverage-clover reports/coverage.xml"

dusk:
	@make exec cmd="php artisan dusk -c phpunit.dusk.xml"

phpunit:
	@make exec cmd="vendor/bin/phpunit -c phpunit.xml --log-junit reports/phpunit.xml --coverage-html reports/coverage --coverage-clover reports/coverage.xml"

check-maintainability:
	@make exec cmd="php artisan maintainability:check --minimum=65"

check-coverage:
	@make exec cmd="php artisan coverage:check --coverage-file=reports/coverage.xml --required-percentage=75"

check-duplications:
	@make exec cmd="php artisan duplication:check --duplication-file=reports/cpd.xml --no-more-times=0"

check-failed-reports:
	@make exec cmd="php artisan errors:view"

phpmetrics:
	@make exec cmd="vendor/bin/phpmetrics --report-html=reports/metrics ."

phpcs:
	@make exec cmd="vendor/bin/phpcs . --standard=PSR2 --report=checkstyle --report-file=reports/checkstyle.xml --ignore=vendor,reports,storage,resources,bootstrap,database,ansible,docker,node_modules,public,listener.js,webpack.mix.js,_ide_helper.php"

phpmd:
	@make exec cmd="vendor/bin/phpmd app,bootstrap,database,routes xml unusedcode,phpmd --exclude app/console,app/exceptions --reportfile reports/pmd.xml "

phpcpd:
	@make exec-true cmd="vendor/bin/phpcpd --log-pmd reports/cpd.xml app bootstrap database config routes --exclude cache bootstrap"

aws-truncate:
	@docker exec tour-service php artisan db:truncate

aws-migrate:
	@docker exec tour-service php artisan migrate --force

aws-seed:
	@docker exec tour-service php artisan db:seed --force

aws-config-cache:
	@docker exec -i -t tour-service php artisan config:cache

aws-ssh:
	@docker exec -i -t tour-service /bin/bash

sync-tours:
	@make exec cmd="php artisan sync:tours"

aws-sync-tours:
	@docker exec tour-service php artisan sync:tours

aws-cache-permissions:
	@docker exec tour-service chmod -R 777 bootstrap/cache/
